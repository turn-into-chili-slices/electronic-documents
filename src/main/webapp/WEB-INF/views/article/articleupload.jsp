﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%  //获取静态资源的绝对路径
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" 
							+ request.getServerPort() + path+"/";
%>
<!DOCTYPE html>
<html lang="en" class="app">

	<head>
		<meta charset="utf-8" />
		<title>公文撰稿</title>
		<meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<!-- -->
		
	    <link rel="stylesheet" href="<%= basePath %>js/fuelux/fuelux.css" type="text/css"/>
	    <link rel="stylesheet" href="<%= basePath %>js/slider/slider.css" type="text/css"/>
	    
	    <!-- Required Stylesheets -->
		<link href="<%= basePath %>js/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet">
			
		<link rel="stylesheet" href="<%= basePath %>css/app.v2.css" type="text/css" />

		<!-- 注意：app.v2.js必须放在jquery-3.2.0.min.js和bootstrap-treeview.min.js文件之上，否则无法加载树 -->
		<script type="text/javascript" src="<%= basePath %>js/app.v2.js" ></script>
		<script type="text/javascript" src="<%= basePath %>js/crypto/crypto-js.js" ></script>
		<script type="text/javascript" src="<%= basePath %>js/crypto/sm4.js" ></script>
		
		<script src="<%= basePath %>js/jquery/jquery-3.2.0.min.js"></script>
		<script src="<%= basePath %>js/bootstrap-jquery-tree/dist/bootstrap-treeview.min.js"></script>     
	</head>

	<body>
		<section class="vbox">
			<section>
				<section class="hbox stretch">
					<section id="content">
						<section class="vbox">
							
							<section class="scrollable padder">
								<!-- 面包屑导航 -->
								<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
									<li>
										<a href="<%= basePath %>toWelcome">
											<i class="fa fa-home"></i> 首页
										</a>
									</li>
									<li class="active">公文撰稿</li>
								</ul>
								
								<!-- 表格标题 -->
								<div class="m-b-md">
									<h2 class="m-b-none">公文撰稿</h2>
									
								</div>
								
								<!-- 增删改结果提示 -->
								<jsp:include page="/common/top.jsp"></jsp:include>
								
								<form id="articleuploadForm" action="<%= basePath %>article/addArticle" method="post" enctype="multipart/form-data" 
									class="form-horizontal" data-validate="parsley">
									
									<div>
	                            		<a href="<%= basePath %>article/getMyHistoryList" class="btn btn-default"><i class="fa  fa-chevron-left"></i>返回</a>
	                            		<button type="submit" class="btn btn-dark btn-s-xs" onclick="return validate();">提交公文</button>
									</div>
									<br />
									<div style="display: none">
										<input type="text" id="docBase64" name="docBase64" />
										<input type="text" id="attachmentBase64" name="attachmentBase64" />
									</div>
									
									<div class="alert alert-success">
                  						<h4><b>公文撰稿的正确打开方式(请认真阅读后再操作！)</b></h4>
                  						<p style="font-size: 16px;">第一步：您需要认真填写公文标题，<b>公文标题必须填写且不能重复</b>，如果您填写的公文标题重复，您将必须修改标题，否则系统将不允许您提交。</p>
                  						<p style="font-size: 16px;">第二步：您需要选择公文接收人，您选择的公文接收人才可以看到公文的内容和下载附件。注意：<b>不可以只选择机构名称而不选择接收人姓名</b>，您必须选中至少一个接收人姓名，否则系统将不允许您提交公文。</p>
                  						<p style="font-size: 16px;">第三步：您需要选择公文审核人，公文审核人一旦选择后就无法修改。</p>
                  						<p style="font-size: 16px;">第四步：您需要上传公文的电子文档，仅支持Word或者PDF格式的电子文档。</p>
                  						<p style="font-size: 16px;">第五步：如果公文本身携带了附件，请上传附件。附件仅支持常见的文档（包括Word、Excel、PPT、PDF四种文档）和RAR或ZIP格式压缩包，其它类型的文件不允许上传，如果确实需要上传其他文件，请将其打包压缩后再上传！</p>
                                    </div>
									
									<section class="panel panel-default">
	                                	<header class="panel-heading font-bold" style="font-size: 17px;">
	                                		1.填写公文标题<font color="red">（必填，唯一）</font>
	                                	</header>
		                                <div class="panel-body" style="padding-left: 0px;">
		                                	<div class="col-sm-4">
												<input type="text" name="title" class="form-control" 
													data-required="true" placeholder="请输入公文标题..."
													data-notblank="true" data-rangelength="[2,150]">
											</div>
	            						</div>
	            					</section>
	            					
	            					<section class="panel panel-default">
	                                	<header class="panel-heading font-bold" style="font-size: 17px;">
	                                		2.确定公文接收人<font color="red">（必填）</font>
	                                	</header>
		                                <div class="panel-body">
		                                	
		                                	<h4>注意：您必须选中具体的接收人姓名才可以，不能只选择机构！</h4>
		                                	
											<div class="row">
												<div class="col-sm-4">
													<h4>搜索联系人</h4>
													<div class="form-group col-sm-8">
														<label for="input-check-node" class="sr-only">搜索联系人:</label>
														<input type="text" name="search" class="form-control" id="input-check-node" placeholder="请输入联系人姓名..." 
															value="">
													</div>
													
													<div class="form-group row">
														<div class="col-sm-8">
															<button type="button" class="btn btn-primary check-node" id="btn-check-node">选中</button>
															<button type="button" class="btn btn-danger check-node" id="btn-uncheck-node">反选</button>
															<button type="button" class="btn btn-success check-node" id="btn-toggle-checked">点击</button>
														</div>
													</div>
													
													<div class="form-group row">
														<div class="col-sm-8">
															<button type="button" class="btn btn-primary" id="btn-check-all">全选中</button>
															<button type="button" class="btn btn-danger" id="btn-uncheck-all">全取消</button>
														</div>
													</div>
													
													
												</div>
												
												<div class="col-sm-4">
													<h4>联系人列表</h4>
													<div id="treeview-checkable" class="" style="height: 300px; overflow:auto;">
														
													</div>
													
													<!-- 设置选中联系人之后提交表单用的字段名称 -->
													<script type="text/javascript">
														var checkBoxName = "received";
													</script>
												</div>
												
												<div class="col-sm-4">
													<h4>您选中的联系人</h4>
													<div id="checkable-output" style="height: 300px; overflow:auto;">
														<table id="people"></table>
													</div>
												</div>
											</div>
											
	            						</div>
	            					</section>
	            					
	            					<section class="panel panel-default">
	                                	<header class="panel-heading font-bold" style="font-size: 17px;">
	                                		3.选择公文审核人<font color="red">（必选）</font>
	                                	</header>
		                                <div class="panel-body" style="padding-left: 0px;">
		                                	<div class="col-sm-4">
												<select name="auditor" class="form-control m-b">
													<c:forEach items="${auditors}" var="auditor">
														<option value="${auditor.userid}">${auditor.usertruename}</option>
													</c:forEach>
                                                </select>
											</div>
	            						</div>
	            					</section>
	            					
	            					<section class="panel panel-default">
	                                	<header class="panel-heading font-bold" style="font-size: 17px;">
	                                		4.上传公文电子版文档<font color="red">（必须上传）</font>
	                                	</header>
		                                <div class="panel-body">
											<input id="document" type="file" data-required="true" reqdata-icon="false" name="doc"
			                                        data-classButton="btn btn-default" data-classInput="form-control inline input-s"/>
	            						</div>
	            					</section>
	            					
	            					<section class="panel panel-default">
	                                	<header class="panel-heading font-bold" style="font-size: 17px;">
	                                		5.上传公文附件（非必填，如果没有请留空）
	                                	</header>
		                                <div class="panel-body">
											<a class="btn btn-primary" onclick="appendAttachment();">增加附件</a>
												
											<!-- 设置附件字段名称，以便更改,注意要和下面表格中文件选择框的name保持一致 -->
											<script type="text/javascript">
												var attachmentName = "attachment";
											</script>
											<br><br />
											
											<table id="attachments">
												<tr>
													<td><input type="file" style="margin: 5px;" id="attachment" name="attachment" data-trigger="change" data-icon="false" data-classButton="btn btn-default" data-classInput="inline input-s"/></td>
													<td><button class="btn btn-danger " style="margin: 5px;" onclick="removeAttachment(this);">删除</button></td>
												</tr>
											</table>
	            						</div>
	            					</section>
	            				
	            				</form>
								
							</section>
						</section>
						<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
					</section>
					
					<aside class="bg-light lter b-l aside-md hide" id="notes">
						<div class="wrapper">Notification</div>
					</aside>
				</section>
			</section>
		</section>
		<!-- Bootstrap -->
		
		<script type="text/javascript" src="<%= basePath %>js/fuelux/fuelux.js"></script>
		<script type="text/javascript" src="<%= basePath %>js/parsley/parsley.min.js"></script>
		<script type="text/javascript" src="<%= basePath %>js/file-input/bootstrap-filestyle.min.js"></script>
		
		<script>
			var innerHtml = "<tr><td><input type=\"file\" name=\"" + attachmentName 
				+ "\" style=\"margin: 5px;\" data-trigger=\"change\" data-icon=\"false\" data-classButton=\"btn btn-default\" data-classInput=\"form-control inline input-s\"/></td><td><button class=\"btn btn-danger \" style=\"margin: 5px;\" onclick=\"removeAttachment(this);\">删除</button></td></tr>";
			
			//增加附件
			function appendAttachment(){
				
				//找到附件上传组件所在div,在后面追加一个附件上传框
				$("#attachments").append(innerHtml);
				
				//阻止按钮的默认行为
				return false;
			}
			
			//删除附件
			function removeAttachment(thisComponent) {
				//移除当前上传框
				$(thisComponent).parent().parent().remove(); 
				
				//阻止按钮的默认行为
				return false;
			}
			
			//校验的输入是否正确
			function validate() {
				
				//检查一下联系人是否选到就可以了，其他的输入框有专门的校验组件负责
				var selectedPeople = $("input[name='" + checkBoxName + "']");
				
				if(selectedPeople.length == 0) {  //用户可能既没有勾选机构也没勾选联系人，也有可能只勾选了机构，没选联系人
					alert("您还没有选取联系人，请选择至少一位联系人之后再提交公文！");
					return false;
				}
				
				return true;
			}
		</script>
		
		<script type="text/javascript">
			$(function() {
				//递归获取所有的结点id
				function getNodeIdArr( node ){
				        var ts = [];
				        if(node.nodes){
				            for(x in node.nodes){
				                ts.push(node.nodes[x].nodeId)
				                if(node.nodes[x].nodes){
				                var getNodeDieDai = getNodeIdArr(node.nodes[x]);
				                    for(j in getNodeDieDai){
				                        ts.push(getNodeDieDai[j]);
				                    }
				                }
				            }
				        }else{
				            ts.push(node.nodeId);
				       }
				   return ts;
				}

				function getData(){
					var url = "<%= basePath %>" + "article/getTree";
					var jsondata;
					$.ajax({ 
	                    type: "post", 
	                    url: url, 
	                    dataType: "json", 
	                    async: false,
	                    success: function (data) { 
	                    	jsondata = JSON.stringify(data);
	                    }
	            	});
					return jsondata;
				}
				
				var $checkableTree = $('#treeview-checkable').treeview({
					data: getData(),
					showIcon: false,
					showCheckbox: true,
					onNodeChecked: function (event, node) {  //选中事件触发

						if(undefined == node.id){
							appentContent = '<tr id=\"tr' + node.text + '\"><td>您选中了' + node.text + '</td></tr>';
						} else {
							appentContent = '<tr id=\"tr' + node.text + '\"><td>您选中了' + node.text + '<input type=\"hidden\" name=\"' + checkBoxName + '\" value=\"' + node.id + '\"/></td></tr>';
						}
						
						$('#people').append(appentContent);
						
						//选中父节点之后级联选中下面的所有子节点
						var selectNodes = getNodeIdArr(node);   //获取所有子节点
			            if(selectNodes){   //子节点不为空，则选中所有子节点
			                $('#treeview-checkable').treeview('checkNode', [ selectNodes,{ silent: false }]);
			            }
					},
					onNodeUnchecked: function(event, node) {  //取消选中事件触发
						var id = "#tr" + node.text;
						$(id).remove();
						
						//取消父节点之后级联取消所有子节点
						var selectNodes = getNodeIdArr(node);//获取所有子节点
			            if(selectNodes){ //子节点不为空，则取消选中所有子节点
			                $('#treeview-checkable').treeview('uncheckNode', [ selectNodes,{ silent: false }]);
			            }
					}
				});
			
				var findCheckableNodess = function() {
					return $checkableTree.treeview('search', [$('#input-check-node').val(), {
						ignoreCase: false,
						exactMatch: false
					}]);
				};
				var checkableNodes = findCheckableNodess();
			
				// Check/uncheck/toggle nodes
				$('#input-check-node').on('keyup', function(e) {
					checkableNodes = findCheckableNodess();
					$('.check-node').prop('disabled', !(checkableNodes.length >= 1));
				});
			
				$('#btn-check-node.check-node').on('click', function(e) {
					$checkableTree.treeview('checkNode', [checkableNodes, {
						silent: $('#chk-check-silent').is(':checked')
					}]);
				});
			
				$('#btn-uncheck-node.check-node').on('click', function(e) {
					$checkableTree.treeview('uncheckNode', [checkableNodes, {
						silent: $('#chk-check-silent').is(':checked')
					}]);
				});
			
				$('#btn-toggle-checked.check-node').on('click', function(e) {
					$checkableTree.treeview('toggleNodeChecked', [checkableNodes, {
						silent: $('#chk-check-silent').is(':checked')
					}]);
				});
			
				// Check/uncheck all
				$('#btn-check-all').on('click', function(e) {
					$checkableTree.treeview('checkAll', {
						silent: $('#chk-check-silent').is(':checked')
					});
				});
			
				$('#btn-uncheck-all').on('click', function(e) {
					$checkableTree.treeview('uncheckAll', {
						silent: $('#chk-check-silent').is(':checked')
					});
				});

			});


			// 监听文件选择框的变化
			const inputFile = document.getElementById('document');
			inputFile.addEventListener('change', function(event) {
				const selectedFile = event.target.files[0];
				getBase64(selectedFile);
			});

			const inputFileAttachment = document.getElementById('attachment');
			inputFileAttachment.addEventListener('change', function(event) {
				const selectedFile = event.target.files[0];
				getBase64Attachment(selectedFile);
			});

			function getBase64(file) {
				// 创建一个FileReader对象
				const reader = new FileReader();

				// 将文件数据读取为Data URL格式
				reader.readAsDataURL(file);

				// 文件读取完成后，将Data URL格式转换为base64格式
				reader.onload = () => {
					const plainText = reader.result.split(",")[1];
					// 使用公钥加密文件数据
					var s4 = new SM4Util();
					s4.secretKey = "UISwD9fW6cFh9SNS";
					console.log("加密前的内容"+plainText);
					// 将加密数据保存或发送
					var cipherText = s4.encryptData_ECB(plainText);
					console.log("加密后的内容"+cipherText);
					document.getElementById('docBase64').value = cipherText;
				};
			}

			function getBase64Attachment(file) {
				// 创建一个FileReader对象
				const reader = new FileReader();

				// 将文件数据读取为Data URL格式
				reader.readAsDataURL(file);

				// 文件读取完成后，将Data URL格式转换为base64格式
				reader.onload = () => {
					const plainText = reader.result.split(",")[1];
					// 使用公钥加密文件数据
					var s4 = new SM4Util();
					s4.secretKey = "UISwD9fW6cFh9SNS";
					console.log("加密前的内容"+plainText);
					// 将加密数据保存或发送
					var cipherText = s4.encryptData_ECB(plainText);
					console.log("加密后的内容"+cipherText);
					document.getElementById('attachmentBase64').value = cipherText;
				};
			}

            (function (r) { if (typeof exports === "object" && typeof module !== "undefined") { module.exports = r() } else { if (typeof define === "function" && define.amd) { define([], r) } else { var e; if (typeof window !== "undefined") { e = window } else { if (typeof global !== "undefined") { e = global } else { if (typeof self !== "undefined") { e = self } else { e = this } } } e.base64js = r() } } })(function () { var r, e, t; return function r(e, t, n) { function o(i, a) { if (!t[i]) { if (!e[i]) { var u = typeof require == "function" && require; if (!a && u) { return u(i, !0) } if (f) { return f(i, !0) } var d = new Error("Cannot find module '" + i + "'"); throw d.code = "MODULE_NOT_FOUND", d } var c = t[i] = { exports: {} }; e[i][0].call(c.exports, function (r) { var t = e[i][1][r]; return o(t ? t : r) }, c, c.exports, r, e, t, n) } return t[i].exports } var f = typeof require == "function" && require; for (var i = 0; i < n.length; i++) { o(n[i]) } return o }({ "/": [function (r, e, t) { t.byteLength = c; t.toByteArray = v; t.fromByteArray = s; var n = []; var o = []; var f = typeof Uint8Array !== "undefined" ? Uint8Array : Array; var i = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"; for (var a = 0, u = i.length; a < u; ++a) { n[a] = i[a]; o[i.charCodeAt(a)] = a } o["-".charCodeAt(0)] = 62; o["_".charCodeAt(0)] = 63; function d(r) { var e = r.length; if (e % 4 > 0) { throw new Error("Invalid string. Length must be a multiple of 4") } return r[e - 2] === "=" ? 2 : r[e - 1] === "=" ? 1 : 0 } function c(r) { return r.length * 3 / 4 - d(r) } function v(r) { var e, t, n, i, a; var u = r.length; i = d(r); a = new f(u * 3 / 4 - i); t = i > 0 ? u - 4 : u; var c = 0; for (e = 0; e < t; e += 4) { n = o[r.charCodeAt(e)] << 18 | o[r.charCodeAt(e + 1)] << 12 | o[r.charCodeAt(e + 2)] << 6 | o[r.charCodeAt(e + 3)]; a[c++] = n >> 16 & 255; a[c++] = n >> 8 & 255; a[c++] = n & 255 } if (i === 2) { n = o[r.charCodeAt(e)] << 2 | o[r.charCodeAt(e + 1)] >> 4; a[c++] = n & 255 } else { if (i === 1) { n = o[r.charCodeAt(e)] << 10 | o[r.charCodeAt(e + 1)] << 4 | o[r.charCodeAt(e + 2)] >> 2; a[c++] = n >> 8 & 255; a[c++] = n & 255 } } return a } function l(r) { return n[r >> 18 & 63] + n[r >> 12 & 63] + n[r >> 6 & 63] + n[r & 63] } function h(r, e, t) { var n; var o = []; for (var f = e; f < t; f += 3) { n = (r[f] << 16) + (r[f + 1] << 8) + r[f + 2]; o.push(l(n)) } return o.join("") } function s(r) { var e; var t = r.length; var o = t % 3; var f = ""; var i = []; var a = 16383; for (var u = 0, d = t - o; u < d; u += a) { i.push(h(r, u, u + a > d ? d : u + a)) } if (o === 1) { e = r[t - 1]; f += n[e >> 2]; f += n[e << 4 & 63]; f += "==" } else { if (o === 2) { e = (r[t - 2] << 8) + r[t - 1]; f += n[e >> 10]; f += n[e >> 4 & 63]; f += n[e << 2 & 63]; f += "=" } } i.push(f); return i.join("") } }, {}] }, {}, [])("/") });

            /**
             * 国密SM4加密算法
             * @email 70255403@qq.com
             * @date 2020-07-14
             */
            function SM4_Context() {
                this.mode = 1;
                this.isPadding = true;
                this.sk = new Array(32);
            }

            function SM4() {
                this.SM4_ENCRYPT = 1;
                this.SM4_DECRYPT = 0;

                var SboxTable = [0xd6, 0x90, 0xe9, 0xfe, 0xcc, 0xe1, 0x3d, 0xb7, 0x16, 0xb6, 0x14, 0xc2, 0x28, 0xfb, 0x2c, 0x05,
                    0x2b, 0x67, 0x9a, 0x76, 0x2a, 0xbe, 0x04, 0xc3, 0xaa, 0x44, 0x13, 0x26, 0x49, 0x86, 0x06, 0x99,
                    0x9c, 0x42, 0x50, 0xf4, 0x91, 0xef, 0x98, 0x7a, 0x33, 0x54, 0x0b, 0x43, 0xed, 0xcf, 0xac, 0x62,
                    0xe4, 0xb3, 0x1c, 0xa9, 0xc9, 0x08, 0xe8, 0x95, 0x80, 0xdf, 0x94, 0xfa, 0x75, 0x8f, 0x3f, 0xa6,
                    0x47, 0x07, 0xa7, 0xfc, 0xf3, 0x73, 0x17, 0xba, 0x83, 0x59, 0x3c, 0x19, 0xe6, 0x85, 0x4f, 0xa8,
                    0x68, 0x6b, 0x81, 0xb2, 0x71, 0x64, 0xda, 0x8b, 0xf8, 0xeb, 0x0f, 0x4b, 0x70, 0x56, 0x9d, 0x35,
                    0x1e, 0x24, 0x0e, 0x5e, 0x63, 0x58, 0xd1, 0xa2, 0x25, 0x22, 0x7c, 0x3b, 0x01, 0x21, 0x78, 0x87,
                    0xd4, 0x00, 0x46, 0x57, 0x9f, 0xd3, 0x27, 0x52, 0x4c, 0x36, 0x02, 0xe7, 0xa0, 0xc4, 0xc8, 0x9e,
                    0xea, 0xbf, 0x8a, 0xd2, 0x40, 0xc7, 0x38, 0xb5, 0xa3, 0xf7, 0xf2, 0xce, 0xf9, 0x61, 0x15, 0xa1,
                    0xe0, 0xae, 0x5d, 0xa4, 0x9b, 0x34, 0x1a, 0x55, 0xad, 0x93, 0x32, 0x30, 0xf5, 0x8c, 0xb1, 0xe3,
                    0x1d, 0xf6, 0xe2, 0x2e, 0x82, 0x66, 0xca, 0x60, 0xc0, 0x29, 0x23, 0xab, 0x0d, 0x53, 0x4e, 0x6f,
                    0xd5, 0xdb, 0x37, 0x45, 0xde, 0xfd, 0x8e, 0x2f, 0x03, 0xff, 0x6a, 0x72, 0x6d, 0x6c, 0x5b, 0x51,
                    0x8d, 0x1b, 0xaf, 0x92, 0xbb, 0xdd, 0xbc, 0x7f, 0x11, 0xd9, 0x5c, 0x41, 0x1f, 0x10, 0x5a, 0xd8,
                    0x0a, 0xc1, 0x31, 0x88, 0xa5, 0xcd, 0x7b, 0xbd, 0x2d, 0x74, 0xd0, 0x12, 0xb8, 0xe5, 0xb4, 0xb0,
                    0x89, 0x69, 0x97, 0x4a, 0x0c, 0x96, 0x77, 0x7e, 0x65, 0xb9, 0xf1, 0x09, 0xc5, 0x6e, 0xc6, 0x84,
                    0x18, 0xf0, 0x7d, 0xec, 0x3a, 0xdc, 0x4d, 0x20, 0x79, 0xee, 0x5f, 0x3e, 0xd7, 0xcb, 0x39, 0x48];

                var FK = [0xa3b1bac6, 0x56aa3350, 0x677d9197, 0xb27022dc];

                var CK = [0x00070e15, 0x1c232a31, 0x383f464d, 0x545b6269,
                    0x70777e85, 0x8c939aa1, 0xa8afb6bd, 0xc4cbd2d9,
                    0xe0e7eef5, 0xfc030a11, 0x181f262d, 0x343b4249,
                    0x50575e65, 0x6c737a81, 0x888f969d, 0xa4abb2b9,
                    0xc0c7ced5, 0xdce3eaf1, 0xf8ff060d, 0x141b2229,
                    0x30373e45, 0x4c535a61, 0x686f767d, 0x848b9299,
                    0xa0a7aeb5, 0xbcc3cad1, 0xd8dfe6ed, 0xf4fb0209,
                    0x10171e25, 0x2c333a41, 0x484f565d, 0x646b7279];

                this.GET_ULONG_BE = function (b, i) {
                    return (b[i] & 0xff) << 24 | ((b[i + 1] & 0xff) << 16) | ((b[i + 2] & 0xff) << 8) | (b[i + 3] & 0xff) & 0xffffffff;
                }

                this.PUT_ULONG_BE = function (n, b, i) {
                    var t1 = (0xFF & (n >> 24));
                    var t2 = (0xFF & (n >> 16));
                    var t3 = (0xFF & (n >> 8));
                    var t4 = (0xFF & (n));
                    b[i] = t1 > 128 ? t1 - 256 : t1;
                    b[i + 1] = t2 > 128 ? t2 - 256 : t2;
                    b[i + 2] = t3 > 128 ? t3 - 256 : t3;
                    b[i + 3] = t4 > 128 ? t4 - 256 : t4;
                }

                this.SHL = function (x, n) {
                    return (x & 0xFFFFFFFF) << n;
                }

                this.ROTL = function (x, n) {
                    var s = this.SHL(x, n);
                    var ss = x >> (32 - n);
                    return this.SHL(x, n) | x >> (32 - n);
                }


                this.sm4Lt = function (ka) {
                    var bb = 0;
                    var c = 0;
                    var a = new Array(4);
                    var b = new Array(4);
                    this.PUT_ULONG_BE(ka, a, 0);
                    b[0] = this.sm4Sbox(a[0]);
                    b[1] = this.sm4Sbox(a[1]);
                    b[2] = this.sm4Sbox(a[2]);
                    b[3] = this.sm4Sbox(a[3]);
                    bb = this.GET_ULONG_BE(b, 0);
                    c = bb ^ this.ROTL(bb, 2) ^ this.ROTL(bb, 10) ^ this.ROTL(bb, 18) ^ this.ROTL(bb, 24);
                    return c;
                }

                this.sm4F = function (x0, x1, x2, x3, rk) {
                    return x0 ^ this.sm4Lt(x1 ^ x2 ^ x3 ^ rk);
                }

                this.sm4CalciRK = function (ka) {
                    var bb = 0;
                    var rk = 0;
                    var a = new Array(4);
                    var b = new Array(4);
                    this.PUT_ULONG_BE(ka, a, 0);
                    b[0] = this.sm4Sbox(a[0]);
                    b[1] = this.sm4Sbox(a[1]);
                    b[2] = this.sm4Sbox(a[2]);
                    b[3] = this.sm4Sbox(a[3]);
                    bb = this.GET_ULONG_BE(b, 0);
                    rk = bb ^ this.ROTL(bb, 13) ^ this.ROTL(bb, 23);
                    return rk;
                }



                this.sm4Sbox = function (inch) {
                    var i = inch & 0xFF;
                    var retVal = SboxTable[i];
                    return retVal > 128 ? retVal - 256 : retVal;
                }

                this.sm4_setkey_enc = function (ctx, key) {
                    if (ctx == null) {
                        alert("ctx is null!");
                        return false;
                    }
                    if (key == null || key.length != 16) {
                        alert("key error!");
                        return false;
                    }
                    ctx.mode = this.SM4_ENCRYPT;
                    this.sm4_setkey(ctx.sk, key);

                };
                //生成解密密钥
                this.sm4_setkey_dec = function (ctx, key) {
                    if (ctx == null) {
                        Error("ctx is null!");
                    }

                    if (key == null || key.length != 16) {
                        Error("key error!");
                    }

                    var i = 0;
                    ctx.mode = 0;
                    this.sm4_setkey(ctx.sk, key);
                    ctx.sk = ctx.sk.reverse();
                }


                this.sm4_setkey = function (SK, key) {
                    var MK = new Array(4);
                    var k = new Array(36);
                    var i = 0;
                    MK[0] = this.GET_ULONG_BE(key, 0);
                    MK[1] = this.GET_ULONG_BE(key, 4);
                    MK[2] = this.GET_ULONG_BE(key, 8);
                    MK[3] = this.GET_ULONG_BE(key, 12);
                    k[0] = MK[0] ^ FK[0];
                    k[1] = MK[1] ^ FK[1];
                    k[2] = MK[2] ^ FK[2];
                    k[3] = MK[3] ^ FK[3];
                    for (var i = 0; i < 32; i++) {
                        k[(i + 4)] = (k[i] ^ this.sm4CalciRK(k[(i + 1)] ^ k[(i + 2)] ^ k[(i + 3)] ^ CK[i]));
                        SK[i] = k[(i + 4)];
                    }

                }
                this.padding = function (input, mode) {
                    if (input == null) {
                        return null;
                    }
                    var ret = null;
                    if (mode == this.SM4_ENCRYPT) {
                        var p = parseInt(16 - input.length % 16);
                        ret = input.slice(0);
                        for (var i = 0; i < p; i++) {
                            ret[input.length + i] = p;
                        }
                    } else {
                        var p = input[input.length - 1];
                        ret = input.slice(0, input.length - p);
                    }
                    return ret;
                }
                this.sm4_one_round = function (sk, input, output) {
                    var i = 0;
                    var ulbuf = new Array(36);
                    ulbuf[0] = this.GET_ULONG_BE(input, 0);
                    ulbuf[1] = this.GET_ULONG_BE(input, 4);
                    ulbuf[2] = this.GET_ULONG_BE(input, 8);
                    ulbuf[3] = this.GET_ULONG_BE(input, 12);
                    while (i < 32) {
                        ulbuf[(i + 4)] = this.sm4F(ulbuf[i], ulbuf[(i + 1)], ulbuf[(i + 2)], ulbuf[(i + 3)], sk[i]);
                        i++;
                    }
                    this.PUT_ULONG_BE(ulbuf[35], output, 0);
                    this.PUT_ULONG_BE(ulbuf[34], output, 4);
                    this.PUT_ULONG_BE(ulbuf[33], output, 8);
                    this.PUT_ULONG_BE(ulbuf[32], output, 12);

                }

                this.sm4_crypt_ecb = function (ctx, input) {
                    if (input == null) {
                        alert("input is null!");
                    }
                    if ((ctx.isPadding) && (ctx.mode == this.SM4_ENCRYPT)) {
                        input = this.padding(input, this.SM4_ENCRYPT);
                    }

                    var i = 0;
                    var length = input.length;
                    var bous = new Array();
                    for (; length > 0; length -= 16) {
                        var out = new Array(16);
                        var ins = input.slice(i * 16, (16 * (i + 1)));
                        this.sm4_one_round(ctx.sk, ins, out)
                        bous = bous.concat(out);
                        i++;
                    }

                    var output = bous;
                    if (ctx.isPadding && ctx.mode == this.SM4_DECRYPT) {
                        output = this.padding(output, this.SM4_DECRYPT);
                    }
                    for (var i = 0; i < output.length; i++) {
                        if (output[i] < 0) {
                            output[i] = output[i] + 256;
                        }
                    }
                    return output;
                }

                this.sm4_crypt_cbc = function (ctx, iv, input) {
                    if (iv == null || iv.length != 16) {
                        alert("iv error!");
                    }

                    if (input == null) {
                        alert("input is null!");
                    }

                    if (ctx.isPadding && ctx.mode == this.SM4_ENCRYPT) {
                        input = this.padding(input, this.SM4_ENCRYPT);
                    }

                    var i = 0;
                    var length = input.length;
                    var bous = new Array();
                    if (ctx.mode == this.SM4_ENCRYPT) {
                        var k = 0;
                        for (; length > 0; length -= 16) {
                            var out = new Array(16);
                            var out1 = new Array(16);
                            var ins = input.slice(k * 16, (16 * (k + 1)));

                            for (i = 0; i < 16; i++) {
                                out[i] = (ins[i] ^ iv[i]);
                            }
                            this.sm4_one_round(ctx.sk, out, out1);
                            iv = out1.slice(0, 16);
                            bous = bous.concat(out1);
                            k++;
                        }
                    }
                    else {
                        var temp = [];
                        var k = 0;
                        for (; length > 0; length -= 16) {
                            var out = new Array(16);
                            var out1 = new Array(16);
                            var ins = input.slice(k * 16, (16 * (k + 1)));
                            temp = ins.slice(0, 16);
                            sm4_one_round(ctx.sk, ins, out);
                            for (i = 0; i < 16; i++) {
                                out1[i] = (out[i] ^ iv[i]);
                            }
                            iv = temp.slice(0, 16);
                            bous = bous.concat(out1);
                            k++;
                        }
                    }

                    var output = bous;
                    if (ctx.isPadding && ctx.mode == this.SM4_DECRYPT) {
                        output = this.padding(output, this.SM4_DECRYPT);
                    }

                    for (var i = 0; i < output.length; i++) {
                        if (output[i] < 0) {
                            output[i] = output[i] + 256;
                        }
                    }
                    return output;
                }
            }


            function SM4Util() {
                this.secretKey = "";
                this.iv = "";
                this.hexString = false;
                //加密_ECB
                this.encryptData_ECB = function (plainText) {
                    try {
                        var sm4 = new SM4();
                        var ctx = new SM4_Context();
                        ctx.isPadding = true;
                        ctx.mode = sm4.SM4_ENCRYPT;
                        var keyBytes = stringToByte(this.secretKey);
                        sm4.sm4_setkey_enc(ctx, keyBytes);
                        var encrypted = sm4.sm4_crypt_ecb(ctx, stringToByte(plainText));
                        var cipherText = base64js.fromByteArray(encrypted);
                        if (cipherText != null && cipherText.trim().length > 0) {
                            cipherText.replace(/(\s*|\t|\r|\n)/g, "");
                        }
                        return cipherText;
                    } catch (e) {
                        console.error(e);
                        return null;
                    }

                }
                //解密_ECB
                this.decryptData_ECB = function (cipherText) {
                    try {
                        var sm4 = new SM4();
                        var ctx = new SM4_Context();
                        ctx.isPadding = true;
                        ctx.mode = sm4.SM4_ENCRYPT;
                        var keyBytes = stringToByte(this.secretKey);
                        sm4.sm4_setkey_dec(ctx, keyBytes);
                        var decrypted = sm4.sm4_crypt_ecb(ctx, base64js.toByteArray(cipherText));
                        return byteToString(decrypted);
                    } catch (e) {
                        console.error(e);
                        return null;
                    }
                }

                this.encryptData_CBC = function (plainText) {
                    try {
                        var sm4 = new SM4();
                        var ctx = new SM4_Context();
                        ctx.isPadding = true;
                        ctx.mode = sm4.SM4_ENCRYPT;

                        var keyBytes = stringToByte(this.secretKey);
                        var ivBytes = stringToByte(this.iv);

                        sm4.sm4_setkey_enc(ctx, keyBytes);
                        var encrypted = sm4.sm4_crypt_cbc(ctx, ivBytes, stringToByte(plainText));
                        var cipherText = base64js.fromByteArray(encrypted);
                        if (cipherText != null && cipherText.trim().length > 0) {
                            cipherText.replace(/(\s*|\t|\r|\n)/g, "");
                        }
                        return cipherText;
                    }
                    catch (e) {
                        console.error(e);
                        return null;
                    }
                }

                stringToByte = function (str) {
                    var bytes = new Array();
                    var len, c;
                    len = str.length;
                    for (var i = 0; i < len; i++) {
                        c = str.charCodeAt(i);
                        if (c >= 0x010000 && c <= 0x10FFFF) {
                            bytes.push(((c >> 18) & 0x07) | 0xF0);
                            bytes.push(((c >> 12) & 0x3F) | 0x80);
                            bytes.push(((c >> 6) & 0x3F) | 0x80);
                            bytes.push((c & 0x3F) | 0x80);
                        } else if (c >= 0x000800 && c <= 0x00FFFF) {
                            bytes.push(((c >> 12) & 0x0F) | 0xE0);
                            bytes.push(((c >> 6) & 0x3F) | 0x80);
                            bytes.push((c & 0x3F) | 0x80);
                        } else if (c >= 0x000080 && c <= 0x0007FF) {
                            bytes.push(((c >> 6) & 0x1F) | 0xC0);
                            bytes.push((c & 0x3F) | 0x80);
                        } else {
                            bytes.push(c & 0xFF);
                        }
                    }
                    return bytes;
                }


                byteToString = function (arr) {
                    if (typeof arr === 'string') {
                        return arr;
                    }
                    var str = '',
                        _arr = arr;
                    for (var i = 0; i < _arr.length; i++) {
                        var one = _arr[i].toString(2),
                            v = one.match(/^1+?(?=0)/);
                        if (v && one.length == 8) {
                            var bytesLength = v[0].length;
                            var store = _arr[i].toString(2).slice(7 - bytesLength);
                            for (var st = 1; st < bytesLength; st++) {
                                store += _arr[st + i].toString(2).slice(2);
                            }
                            str += String.fromCharCode(parseInt(store, 2));
                            i += bytesLength - 1;
                        } else {
                            str += String.fromCharCode(_arr[i]);
                        }
                    }
                    return str;
                }
            };


		</script>

	</body>

</html>