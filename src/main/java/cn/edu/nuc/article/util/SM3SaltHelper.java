package cn.edu.nuc.article.util;

import org.bouncycastle.crypto.digests.SM3Digest;
import org.bouncycastle.util.encoders.Hex;

import java.security.Security;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class SM3SaltHelper {
    public static void main(String[] args) {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        // 原始数据
        byte[] data = "Hello, World!".getBytes();

        // 生成随机的盐值
        byte[] salt = generateSalt();

        // 将原始数据与盐值拼接
        byte[] dataWithSalt = concatBytes(data, salt);

        // 计算SM3哈希值
        byte[] hash = calculateHash(dataWithSalt);

        // 将盐值和哈希值转换为十六进制字符串
        String saltHex = bytesToHex(salt);
        String hashHex = bytesToHex(hash);

        System.out.println("Salt: " + saltHex);
        System.out.println("Hash: " + hashHex);
    }

    public static String encrypt(String paramStr,byte[]  salt){
        Map<String,String> resultMap=new HashMap<>();
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        // 原始数据
        byte[] data = paramStr.getBytes();

        // 将原始数据与盐值拼接
        byte[] dataWithSalt = concatBytes(data, salt);

        // 计算SM3哈希值
        byte[] hash = calculateHash(dataWithSalt);

        // 将盐值和哈希值转换为十六进制字符串
        String hashHex = bytesToHex(hash);
        return  hashHex;
    }


    public static String entryptSM3Password(String plainPassword) {
        byte[] bytesSalt = generateSalt();
        String sm3Password= encrypt(plainPassword,bytesSalt);
        return bytesToHex(bytesSalt)+sm3Password;
    }

    public static byte[] generateSalt() {
        byte[] salt = new byte[8];
        new Random().nextBytes(salt);
        return salt;
    }

    private static byte[] concatBytes(byte[] a, byte[] b) {
        byte[] result = Arrays.copyOf(a, a.length + b.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

    private static byte[] calculateHash(byte[] input) {
        SM3Digest digest = new SM3Digest();
        digest.update(input, 0, input.length);
        byte[] result = new byte[digest.getDigestSize()];
        digest.doFinal(result, 0);
        return result;
    }

    public static String bytesToHex(byte[] bytes) {
        return Hex.toHexString(bytes);
    }

    public static byte[]  HexTobytes(String hexStr) {

        return Hex.decode(hexStr);
    }




}
